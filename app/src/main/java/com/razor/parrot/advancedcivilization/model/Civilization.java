package com.razor.parrot.advancedcivilization.model;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Parcelable;
import android.os.Parcel;

import java.util.ArrayList;

public class Civilization implements Parcelable {
    public String name;
    public int [] AST;
    public int color;
    public Bitmap pawnImg;
    public Bitmap cityImg;
    public Bitmap shipImg;
    public ArrayList<Area> startingAreas;

    public Civilization(String name, int[] AST, int color, Bitmap pawnImg, Bitmap cityImg, Bitmap shipImg, ArrayList<Area> startingAreas) {
        this.name = name;
        this.AST = AST;
        this.color = color;
        this.pawnImg = pawnImg;
        this.cityImg = cityImg;
        this.shipImg=shipImg;
        this.startingAreas = startingAreas;
    }

    public Civilization(){
    };

    protected Civilization(Parcel in) {
        name = in.readString();
        color = in.readInt();
        pawnImg = (Bitmap) in.readValue(Bitmap.class.getClassLoader());
        cityImg = (Bitmap) in.readValue(Bitmap.class.getClassLoader());
        shipImg = (Bitmap) in.readValue(Bitmap.class.getClassLoader());
        if (in.readByte() == 0x01) {
            startingAreas = new ArrayList<Area>();
            in.readList(startingAreas, Area.class.getClassLoader());
        } else {
            startingAreas = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeInt(color);
        dest.writeValue(pawnImg);
        dest.writeValue(cityImg);
        dest.writeValue(shipImg);
        if (startingAreas == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(startingAreas);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Civilization> CREATOR = new Parcelable.Creator<Civilization>() {
        @Override
        public Civilization createFromParcel(Parcel in) {
            return new Civilization(in);
        }

        @Override
        public Civilization[] newArray(int size) {
            return new Civilization[size];
        }
    };
}
