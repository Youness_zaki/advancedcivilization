package com.razor.parrot.advancedcivilization.view_and_controller;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


import com.razor.parrot.advancedcivilization.model.Game;
import com.razor.parrot.advancedcivilization.model.Player;
import com.razor.parrot.advancedcivilization.R;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class MainCartesActivity extends AppCompatActivity {

    //Layout Objects
    TextView TextCards;
    Button Player1;
    Button Player2;
    Button Player3;
    Button Player4;
    Button Player5;
    Button Player6;
    Button Player7;
    Button Player8;
    TextView CardsofPlayer;

    //Model
    int NbPlayers;
    int game;
    int cartes;
    int map;
    int area;
    int Player;
    //Player activePlayer;
    //ArrayList astOrder;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_cartes);

        //initialisation
        getIntent().getIntExtra("NbPlayers",0);








        Player1 = (Button) findViewById (R.id.Player1);
        Player2 = (Button) findViewById (R.id.Player2);
        Player3 = (Button) findViewById (R.id.Player3);
        Player4 = (Button) findViewById (R.id.Player4);
        Player5 = (Button) findViewById (R.id.Player5);
        Player6 = (Button) findViewById (R.id.Player6);
        Player7 = (Button) findViewById (R.id.Player7);
        Player8 = (Button) findViewById (R.id.Player8);
        CardsofPlayer=(TextView)findViewById(R.id.Cardsofplayer);





        if (NbPlayers < 2) {
            Player1.setVisibility(Player1.VISIBLE);
            Player1.setEnabled(true);


        }

        else if (NbPlayers < 3) {
            Player1.setVisibility(Player1.VISIBLE);
            Player1.setEnabled(true);
            Player2.setVisibility(Player2.VISIBLE);
            Player2.setEnabled(true);
        }

        else if (NbPlayers < 4) {
            Player1.setVisibility(Player1.VISIBLE);
            Player1.setEnabled(true);
            Player2.setVisibility(Player2.VISIBLE);
            Player2.setEnabled(true);
            Player3.setVisibility(Player3.VISIBLE);
            Player3.setEnabled(true);
        }

        else if (NbPlayers < 5) {
            Player1.setVisibility(Player1.VISIBLE);
            Player1.setEnabled(true);
            Player2.setVisibility(Player2.VISIBLE);
            Player2.setEnabled(true);
            Player3.setVisibility(Player3.VISIBLE);
            Player3.setEnabled(true);
            Player4.setVisibility(Player4.VISIBLE);
            Player4.setEnabled(true);

        }

        else if (NbPlayers < 6) {
            Player1.setVisibility(Player1.VISIBLE);
            Player1.setEnabled(true);
            Player2.setVisibility(Player2.VISIBLE);
            Player2.setEnabled(true);
            Player3.setVisibility(Player3.VISIBLE);
            Player3.setEnabled(true);
            Player4.setVisibility(Player4.VISIBLE);
            Player4.setEnabled(true);
            Player5.setVisibility(Player5.VISIBLE);
            Player5.setEnabled(true);

        }

        else if (NbPlayers < 7) {
            Player1.setVisibility(Player1.VISIBLE);
            Player1.setEnabled(true);
            Player2.setVisibility(Player2.VISIBLE);
            Player2.setEnabled(true);
            Player3.setVisibility(Player3.VISIBLE);
            Player3.setEnabled(true);
            Player4.setVisibility(Player4.VISIBLE);
            Player4.setEnabled(true);
            Player5.setVisibility(Player5.VISIBLE);
            Player5.setEnabled(true);
            Player6.setVisibility(Player6.VISIBLE);
            Player6.setEnabled(true);
        }

        else if (NbPlayers < 8) {
            Player1.setVisibility(Player1.VISIBLE);
            Player1.setEnabled(true);
            Player2.setVisibility(Player2.VISIBLE);
            Player2.setEnabled(true);
            Player3.setVisibility(Player3.VISIBLE);
            Player3.setEnabled(true);
            Player4.setVisibility(Player4.VISIBLE);
            Player4.setEnabled(true);
            Player5.setVisibility(Player5.VISIBLE);
            Player5.setEnabled(true);
            Player6.setVisibility(Player6.VISIBLE);
            Player6.setEnabled(true);
            Player7.setVisibility(Player7.VISIBLE);
            Player7.setEnabled(true);
        }

        else if (NbPlayers == 8) {
            Player1.setVisibility(Player1.VISIBLE);
            Player1.setEnabled(true);
            Player2.setVisibility(Player2.VISIBLE);
            Player2.setEnabled(true);
            Player3.setVisibility(Player3.VISIBLE);
            Player3.setEnabled(true);
            Player4.setVisibility(Player4.VISIBLE);
            Player4.setEnabled(true);
            Player5.setVisibility(Player5.VISIBLE);
            Player5.setEnabled(true);
            Player6.setVisibility(Player6.VISIBLE);
            Player6.setEnabled(true);
            Player7.setVisibility(Player7.VISIBLE);
            Player7.setEnabled(true);
            Player8.setVisibility(Player8.VISIBLE);
            Player8.setEnabled(true);
        }







//        Player1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                CardsofPlayer.setText(activePlayer.civilization.name);
//                CardsofPlayer.setVisibility(View.VISIBLE);
//
//            }
//        });

//        Player2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (NbPlayers>2){
//                    Player2.setVisibility(Player2.VISIBLE);
//                    Player2.setEnabled(true);
//                }
//                else {
//                    Player2.setVisibility(Player2.INVISIBLE);
//                    Player2.setEnabled(false);
//
//                }
//
//            }
//        });
//        Player3.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (NbPlayers>3){
//                    Player3.setVisibility(Player3.VISIBLE);
//                    Player3.setEnabled(true);
//                }
//                else {
//                    Player3.setVisibility(Player3.INVISIBLE);
//                    Player3.setEnabled(false);
//
//                }
//
//
//            }
//        });
//
//        Player4.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (NbPlayers>4){
//                    Player4.setVisibility(Player4.VISIBLE);
//                    Player4.setEnabled(true);
//                }
//                else {
//                    Player4.setVisibility(Player4.INVISIBLE);
//                    Player4.setEnabled(false);
//
//                }
//
//            }
//        });
    }




}
