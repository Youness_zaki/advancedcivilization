package com.razor.parrot.advancedcivilization.model;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Parcelable;
import android.os.Parcel;

import com.razor.parrot.advancedcivilization.R;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Stack;
import java.util.*;

/**
 * Created by Salome on 21/04/2018.
 */

public class Game implements Parcelable {

    int nbPlayers;
    public Player[] players;


    //Civilizations
    public Civilization africa = new Civilization();
    public Civilization asia = new Civilization();
    public Civilization assyria = new Civilization();
    public Civilization babylon = new Civilization();
    public Civilization crete = new Civilization();
    public Civilization egypt = new Civilization();
    public Civilization illyria = new Civilization();
    public Civilization italy = new Civilization();
    public Civilization thrace = new Civilization();

    //cartes
    public cartes Ochre = new cartes();
    public cartes Hides = new cartes();
    public cartes Iron = new cartes();
    public cartes Papyrus = new cartes();
    public cartes Salt = new cartes();
    public cartes Timber = new cartes();
    public cartes Grain = new cartes();
    public cartes Oil = new cartes();
    public cartes Cloth = new cartes();
    public cartes Wine = new cartes();
    public cartes Bronze = new cartes();
    public cartes Silver = new cartes();
    public cartes Spices = new cartes();
    public cartes Resin = new cartes();
    public cartes Gems = new cartes();
    public cartes Dye = new cartes();
    public cartes Gold = new cartes();
    public cartes Ivory = new cartes();
    public cartes EarthquakeOrVolcano = new cartes();
    public cartes Treachery = new cartes();
    public cartes Famine = new cartes();
    public cartes Superstition = new cartes();
    public cartes CivilWar = new cartes();
    public cartes SlaveRevolt = new cartes();
    public cartes Flood = new cartes();
    public cartes BarbarianHordes = new cartes();
    public cartes Epidemic = new cartes();
    public cartes CivilDisorder = new cartes();
    public cartes IconoclasmAndHeresy = new cartes();
    public cartes Piracy = new cartes();


    public Stack<cartes> premierepiledecartes = new Stack<>();
    public Stack<cartes> deuxiemepiledecartes = new Stack<>();
    public Stack<cartes> troisiemepiledecartes = new Stack<>();
    public Stack<cartes> quatriemepiledecartes = new Stack<>();
    public Stack<cartes> cinquiemepiledecartes = new Stack<>();
    public Stack<cartes> sixiemepiledecartes = new Stack<>();
    public Stack<cartes> septiemepiledecartes = new Stack<>();
    public Stack<cartes> huitiemepiledecartes = new Stack<>();
    public Stack<cartes> neuviemepiledecartes = new Stack<>();



    public Game(Context c, int nbPlayers, Map map){

        this.nbPlayers = nbPlayers;
        players = new Player[nbPlayers];
        for (int i = 1; i <= nbPlayers; i++){
            players[i-1] = new Player (i);
        }

        //Cartes de commerce

        Ochre.value = 1;
        Ochre.name = "Ochre";
        Ochre.genre = "Commerce";
        Ochre.trade = "Tradable";
        Ochre.imgcarte = R.drawable.ochre_commerce;

        Hides.value = 1;
        Hides.name = "Hides";
        Hides.genre = "Commerce";
        Hides.trade = "Tradable";
        Hides.imgcarte = R.drawable.hides_commerce;

        Iron.value = 2;
        Iron.name = "Iron";
        Iron.genre = "Commerce";
        Iron.trade = "Tradable";
        Iron.imgcarte = R.drawable.iron_commerce;

        Papyrus.value = 2;
        Papyrus.name = "Papyrus";
        Papyrus.genre = "Commerce";
        Papyrus.trade = "Tradable";
        Papyrus.imgcarte = R.drawable.papyrus_commerce;

        Salt.value = 3;
        Salt.name = "Salt";
        Salt.genre = "Commerce";
        Salt.trade = "Tradable";
        Salt.imgcarte = R.drawable.salt_commerce;

        Timber.value = 2;
        Timber.name = "Timber";
        Timber.genre = "Commerce";
        Timber.trade = "Tradable";
        Timber.imgcarte = R.drawable.timber_commerce;

        Grain.value = 4;
        Grain.name = "Grain";
        Grain.genre = "Commerce";
        Grain.trade = "Tradable";
        Grain.imgcarte = R.drawable.grain_commerce;

        Oil.value = 4;
        Oil.name = "Oil";
        Oil.genre = "Commerce";
        Oil.trade = "Tradable";
        Oil.imgcarte = R.drawable.oil_commerce;

        Cloth.value = 5;
        Cloth.name = "Cloth";
        Cloth.genre = "Commerce";
        Cloth.trade = "Tradable";
        Cloth.imgcarte = R.drawable.cloth_commerce;

        Wine.value = 5;
        Wine.name = "Wine";
        Wine.genre = "Commerce";
        Wine.trade = "Tradable";
        Wine.imgcarte = R.drawable.wine_commerce;

        Bronze.value = 6;
        Bronze.name = "Bronze";
        Bronze.genre = "Commerce";
        Bronze.trade = "Tradable";
        Bronze.imgcarte = R.drawable.bronze_commerce;

        Silver.value = 6;
        Silver.name = "Silver";
        Silver.genre = "Commerce";
        Silver.trade = "Tradable";
        Silver.imgcarte = R.drawable.silver_commerce;

        Spices.value = 7;
        Spices.name = "Spices";
        Spices.genre = "Commerce";
        Spices.trade = "Tradable";
        Spices.imgcarte = R.drawable.spice_commerce;

        Resin.value = 7;
        Resin.name = "Resin";
        Resin.genre = "Commerce";
        Resin.trade = "Tradable";
        Resin.imgcarte = R.drawable.resin_commerce;

        Gems.value = 8;
        Gems.name = "Gems";
        Gems.genre = "Commerce";
        Gems.trade = "Tradable";
        Gems.imgcarte = R.drawable.gems_commerce;

        Dye.value = 8;
        Dye.name = "Dye";
        Dye.genre = "Commerce";
        Dye.trade = "Tradable";
        Dye.imgcarte = R.drawable.dye_commerce;

        Gold.value = 9;
        Gold.name = "Gold";
        Gold.genre = "Commerce";
        Gold.trade = "Tradable";
        Gold.imgcarte = R.drawable.gold_commerce;

        Ivory.value = 9;
        Ivory.name = "Ivory";
        Ivory.genre = "Commerce";
        Ivory.trade = "Tradable";
        Ivory.imgcarte = R.drawable.ivory_commerce;

        //Cartes calamites
        EarthquakeOrVolcano.value = 2;
        EarthquakeOrVolcano.name = "EarthquakeOrVolcano";
        EarthquakeOrVolcano.genre = "Calamity";
        EarthquakeOrVolcano.trade = "Non-Tradable";
        EarthquakeOrVolcano.imgcarte = R.drawable.volcaniceruptionorearthquake_calamity;

        Treachery.value = 2;
        Treachery.name = "Treachery";
        Treachery.genre = "Calamity";
        Treachery.trade = "Tradable";
        Treachery.imgcarte = R.drawable.treachery_calamity;

        Famine.value = 3;
        Famine.name = "Famine";
        Famine.genre = "Calamity";
        Famine.trade = "Non-Tradable";
        Famine.imgcarte = R.drawable.famine_calamity;

        Superstition.value = 3;
        Superstition.name = "Superstition";
        Superstition.genre = "Calamity";
        Superstition.trade = "Tradable";
        Superstition.imgcarte = R.drawable.superstition_calamity;

        CivilWar.value = 4;
        CivilWar.name = "CivilWar";
        CivilWar.genre = "Calamity";
        CivilWar.trade = "Non-Tradable";
        CivilWar.imgcarte = R.drawable.civilwar_calamity;

        SlaveRevolt.value = 4;
        SlaveRevolt.name = "SlaveRevolt";
        SlaveRevolt.genre = "Calamity";
        SlaveRevolt.trade = "Tradable";
        SlaveRevolt.imgcarte = R.drawable.slaverevolt_calamity;

        Flood.value = 5;
        Flood.name = "Flood";
        Flood.genre = "Calamity";
        Flood.trade = "Non-Tradable";
        Flood.imgcarte = R.drawable.flood_calamity;

        BarbarianHordes.value = 5;
        BarbarianHordes.name = "BarbarianHordes";
        BarbarianHordes.genre = "Calamity";
        BarbarianHordes.trade = "Tradable";
        BarbarianHordes.imgcarte = R.drawable.barbarianhordes_calamity;

        Epidemic.value = 6;
        Epidemic.name = "Epidemic";
        Epidemic.genre = "Calamity";
        Epidemic.trade = "Tradable";
        Epidemic.imgcarte = R.drawable.epidemic_calamity;

        CivilDisorder.value = 7;
        CivilDisorder.name = "CivilDisorder";
        CivilDisorder.genre = "Calamity";
        CivilDisorder.trade = "Tradable";
        CivilDisorder.imgcarte = R.drawable.civildisorder_calamity;

        IconoclasmAndHeresy.value = 8;
        IconoclasmAndHeresy.name = "Ivory";
        IconoclasmAndHeresy.genre = "Calamity";
        IconoclasmAndHeresy.trade = "Tradable";
        IconoclasmAndHeresy.imgcarte = R.drawable.iconoclasmheresy_calamity;

        Piracy.value = 9;
        Piracy.name = "Piracy";
        Piracy.genre = "Calamity";
        Piracy.trade = "Tradable";
        Piracy.imgcarte = R.drawable.piracy_calamity;



        //premiere pile de carte de commerce
        premierepiledecartes.push(Ochre);
        premierepiledecartes.push(Ochre);
        premierepiledecartes.push(Ochre);
        premierepiledecartes.push(Ochre);
        premierepiledecartes.push(Ochre);
        premierepiledecartes.push(Ochre);
        premierepiledecartes.push(Ochre);
        premierepiledecartes.push(Hides);
        premierepiledecartes.push(Hides);
        premierepiledecartes.push(Hides);
        premierepiledecartes.push(Hides);
        premierepiledecartes.push(Hides);
        premierepiledecartes.push(Hides);
        premierepiledecartes.push(Hides);

        //deuxieme pile de carte de commerce
        deuxiemepiledecartes.push(Iron);
        deuxiemepiledecartes.push(Iron);
        deuxiemepiledecartes.push(Iron);
        deuxiemepiledecartes.push(Iron);
        deuxiemepiledecartes.push(Iron);
        deuxiemepiledecartes.push(Iron);
        deuxiemepiledecartes.push(Iron);
        deuxiemepiledecartes.push(Iron);
        deuxiemepiledecartes.push(Papyrus);
        deuxiemepiledecartes.push(Papyrus);
        deuxiemepiledecartes.push(Papyrus);
        deuxiemepiledecartes.push(Papyrus);
        deuxiemepiledecartes.push(Papyrus);
        deuxiemepiledecartes.push(Papyrus);
        deuxiemepiledecartes.push(Papyrus);
        deuxiemepiledecartes.push(EarthquakeOrVolcano);
        deuxiemepiledecartes.push(Treachery);

        //Troisieme pile de cartes de commerce
        troisiemepiledecartes.push(Salt);
        troisiemepiledecartes.push(Salt);
        troisiemepiledecartes.push(Salt);
        troisiemepiledecartes.push(Salt);
        troisiemepiledecartes.push(Salt);
        troisiemepiledecartes.push(Salt);
        troisiemepiledecartes.push(Salt);
        troisiemepiledecartes.push(Salt);
        troisiemepiledecartes.push(Salt);
        troisiemepiledecartes.push(Timber);
        troisiemepiledecartes.push(Timber);
        troisiemepiledecartes.push(Timber);
        troisiemepiledecartes.push(Timber);
        troisiemepiledecartes.push(Timber);
        troisiemepiledecartes.push(Timber);
        troisiemepiledecartes.push(Timber);
        troisiemepiledecartes.push(Timber);
        troisiemepiledecartes.push(Famine);
        troisiemepiledecartes.push(Superstition);

        //quatrieme pile de cartes de commerce
        quatriemepiledecartes.push(Grain);
        quatriemepiledecartes.push(Grain);
        quatriemepiledecartes.push(Grain);
        quatriemepiledecartes.push(Grain);
        quatriemepiledecartes.push(Grain);
        quatriemepiledecartes.push(Grain);
        quatriemepiledecartes.push(Grain);
        quatriemepiledecartes.push(Grain);
        quatriemepiledecartes.push(Oil);
        quatriemepiledecartes.push(Oil);
        quatriemepiledecartes.push(Oil);
        quatriemepiledecartes.push(Oil);
        quatriemepiledecartes.push(Oil);
        quatriemepiledecartes.push(Oil);
        quatriemepiledecartes.push(Oil);
        quatriemepiledecartes.push(CivilWar);
        quatriemepiledecartes.push(SlaveRevolt);

        //cinquieme pile de cartes de commerce
        cinquiemepiledecartes.push(Cloth);
        cinquiemepiledecartes.push(Cloth);
        cinquiemepiledecartes.push(Cloth);
        cinquiemepiledecartes.push(Cloth);
        cinquiemepiledecartes.push(Cloth);
        cinquiemepiledecartes.push(Cloth);
        cinquiemepiledecartes.push(Cloth);
        cinquiemepiledecartes.push(Wine);
        cinquiemepiledecartes.push(Wine);
        cinquiemepiledecartes.push(Wine);
        cinquiemepiledecartes.push(Wine);
        cinquiemepiledecartes.push(Wine);
        cinquiemepiledecartes.push(Wine);
        cinquiemepiledecartes.push(Flood);
        cinquiemepiledecartes.push(BarbarianHordes);

        //sixieme pile de cartes de commerce
        sixiemepiledecartes.push(Bronze);
        sixiemepiledecartes.push(Bronze);
        sixiemepiledecartes.push(Bronze);
        sixiemepiledecartes.push(Bronze);
        sixiemepiledecartes.push(Bronze);
        sixiemepiledecartes.push(Bronze);
        sixiemepiledecartes.push(Silver);
        sixiemepiledecartes.push(Silver);
        sixiemepiledecartes.push(Silver);
        sixiemepiledecartes.push(Silver);
        sixiemepiledecartes.push(Silver);
        sixiemepiledecartes.push(Epidemic);

        //septieme pile de cartes de commerce
        septiemepiledecartes.push(Spices);
        septiemepiledecartes.push(Spices);
        septiemepiledecartes.push(Spices);
        septiemepiledecartes.push(Spices);
        septiemepiledecartes.push(Spices);
        septiemepiledecartes.push(Spices);
        septiemepiledecartes.push(Resin);
        septiemepiledecartes.push(Resin);
        septiemepiledecartes.push(Resin);
        septiemepiledecartes.push(Resin);
        septiemepiledecartes.push(Resin);
        septiemepiledecartes.push(CivilDisorder);

        //huitieme pile de cartes de commerce
        huitiemepiledecartes.push(Gems);
        huitiemepiledecartes.push(Gems);
        huitiemepiledecartes.push(Gems);
        huitiemepiledecartes.push(Gems);
        huitiemepiledecartes.push(Gems);
        huitiemepiledecartes.push(Dye);
        huitiemepiledecartes.push(Dye);
        huitiemepiledecartes.push(Dye);
        huitiemepiledecartes.push(Dye);
        huitiemepiledecartes.push(IconoclasmAndHeresy);

        //neuvieme pile de cartes de commerce
        neuviemepiledecartes.push(Gold);
        neuviemepiledecartes.push(Gold);
        neuviemepiledecartes.push(Gold);
        neuviemepiledecartes.push(Gold);
        neuviemepiledecartes.push(Gold);
        neuviemepiledecartes.push(Ivory);
        neuviemepiledecartes.push(Ivory);
        neuviemepiledecartes.push(Ivory);
        neuviemepiledecartes.push(Ivory);
        neuviemepiledecartes.push(Piracy);

        //Melange des cartes
        Collections.shuffle(premierepiledecartes);
        Collections.shuffle(deuxiemepiledecartes);
        Collections.shuffle(troisiemepiledecartes);
        Collections.shuffle(quatriemepiledecartes);
        Collections.shuffle(cinquiemepiledecartes);
        Collections.shuffle(sixiemepiledecartes);
        Collections.shuffle(septiemepiledecartes);
        Collections.shuffle(huitiemepiledecartes);
        Collections.shuffle(neuviemepiledecartes);

        africa.name = "Africa";
        italy.name = "Italy";
        illyria.name = "Illyria";
        thrace.name = "Thrace";
        crete.name = "Crete";
        asia.name = "Asia";
        assyria.name = "Assyria";
        babylon.name = "Babylon";
        egypt.name = "Egypt";

        africa.AST = new int[]{1,1,1,1, 2,2,2,3,3,3,4,4,4, 1300, 1600};
        italy.AST = new int[]{1,1,1,1, 2,2,2, 3,3,3, 4,4,4, 1400, 1700};
        illyria.AST = new int[]{1,1,1,1,1, 2,2, 3,3,3, 4,4, 1200, 1500, 1800};
        thrace.AST = new int[]{1,1,1,1,1,2,2,2,3,3,4,4, 1200, 1400, 1700};
        crete.AST = new int[]{1,1,1,1,2,2,2,2,3,3,4,4,4, 1300, 1600};
        asia.AST = new int[]{1,1,1,1,2,2,2,3,3,4,4,4, 1200, 1500, 1800};
        assyria.AST = new int[]{1,1,1,1,2,2,2,3,3,4,4,4,4, 1500, 1800};
        babylon.AST = new int[]{1,1,1,2,2,2,3,3,3,4,4,4,4, 1600, 1900};
        egypt.AST = new int[]{1,1,1,2,2,2,3,3,3,4,4,4,1300, 1600, 1900};

        africa.startingAreas = new ArrayList<> (Arrays.asList (map.areas[24], map.areas[23], map.areas[22], map.areas[18]));
        italy.startingAreas = new ArrayList<>(Arrays.asList (map.areas[29], map.areas[28], map.areas[11]));
        illyria.startingAreas = new ArrayList<>(Arrays.asList (map.areas[30], map.areas[31]));
        thrace.startingAreas = new ArrayList<>(Arrays.asList (map.areas[35]));
        crete.startingAreas = new ArrayList<>(Arrays.asList (map.areas[67], map.areas[68]));
        asia.startingAreas = new ArrayList<>(Arrays.asList (map.areas[37], map.areas[39]));
        assyria.startingAreas = new ArrayList<>(Arrays.asList (map.areas[79], map.areas[80]));
        babylon.startingAreas = new ArrayList<>(Arrays.asList (map.areas[83], map.areas[85], map.areas[125]));
        egypt.startingAreas = new ArrayList<>(Arrays.asList (map.areas[117], map.areas[116], map.areas[115], map.areas[77], map.areas[75], map.areas[27]));

        africa.color = Color.argb (255,132,80,60);
        italy.color = Color.argb (255,236,32,40);
        illyria.color = Color.argb (255,234,230,3);
        thrace.color = Color.argb (255,33,102,73);
        crete.color = Color.argb (255,100,185,92);
        asia.color = Color.argb (255,243,136,36);
        assyria.color = Color.argb (255,77,156,200);
        babylon.color = Color.argb (255,119,193,198);
        egypt.color = Color.argb (255,182,98,221);


        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 8;


        africa.pawnImg = BitmapFactory.decodeResource(c.getResources(), R.drawable.africa_pawn,options);
        italy.pawnImg = BitmapFactory.decodeResource(c.getResources(), R.drawable.italia_pawn,options);
        illyria.pawnImg = BitmapFactory.decodeResource(c.getResources(), R.drawable.illirya_pawn,options);
        thrace.pawnImg = BitmapFactory.decodeResource(c.getResources(), R.drawable.thrace_pawn,options);
        crete.pawnImg = BitmapFactory.decodeResource(c.getResources(), R.drawable.crete_pawn,options);
        asia.pawnImg = BitmapFactory.decodeResource(c.getResources(), R.drawable.egypt_pawn,options);
        assyria.pawnImg = BitmapFactory.decodeResource(c.getResources(), R.drawable.assyria_pawn,options);
        babylon.pawnImg = BitmapFactory.decodeResource(c.getResources(), R.drawable.babylon_pawn,options);
        egypt.pawnImg = BitmapFactory.decodeResource(c.getResources(), R.drawable.egypt_pawn,options);

        africa.cityImg = BitmapFactory.decodeResource(c.getResources(), R.drawable.africa_city,options);
        italy.cityImg = BitmapFactory.decodeResource(c.getResources(), R.drawable.italia_city,options);
        illyria.cityImg = BitmapFactory.decodeResource(c.getResources(), R.drawable.illirya_city,options);
        thrace.cityImg = BitmapFactory.decodeResource(c.getResources(), R.drawable.thrace_city,options);
        crete.cityImg = BitmapFactory.decodeResource(c.getResources(), R.drawable.crete_city,options);
        asia.cityImg = BitmapFactory.decodeResource(c.getResources(), R.drawable.egypt_city,options);
        assyria.cityImg = BitmapFactory.decodeResource(c.getResources(), R.drawable.assyria_city,options);
        babylon.cityImg = BitmapFactory.decodeResource(c.getResources(), R.drawable.babylon_city,options);
        egypt.cityImg = BitmapFactory.decodeResource(c.getResources(), R.drawable.egypt_city,options);
    }

    protected Game(Parcel in) {
        nbPlayers = in.readInt();
        africa = (Civilization) in.readValue(Civilization.class.getClassLoader());
        asia = (Civilization) in.readValue(Civilization.class.getClassLoader());
        assyria = (Civilization) in.readValue(Civilization.class.getClassLoader());
        babylon = (Civilization) in.readValue(Civilization.class.getClassLoader());
        crete = (Civilization) in.readValue(Civilization.class.getClassLoader());
        egypt = (Civilization) in.readValue(Civilization.class.getClassLoader());
        illyria = (Civilization) in.readValue(Civilization.class.getClassLoader());
        italy = (Civilization) in.readValue(Civilization.class.getClassLoader());
        thrace = (Civilization) in.readValue(Civilization.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(nbPlayers);
        dest.writeValue(africa);
        dest.writeValue(asia);
        dest.writeValue(assyria);
        dest.writeValue(babylon);
        dest.writeValue(crete);
        dest.writeValue(egypt);
        dest.writeValue(illyria);
        dest.writeValue(italy);
        dest.writeValue(thrace);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Game> CREATOR = new Parcelable.Creator<Game>() {
        @Override
        public Game createFromParcel(Parcel in) {
            return new Game(in);
        }

        @Override
        public Game[] newArray(int size) {
            return new Game[size];
        }
    };





}

