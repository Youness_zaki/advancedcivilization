package com.razor.parrot.advancedcivilization.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;


public class Area implements Parcelable{
    public int num;
    public boolean hasWater;
    public boolean hasLand;
    public boolean hasVolcano;
    public boolean hasFloodPlain;
    public boolean hasCitySite;
    public boolean hasWhiteCitySite;
    public int maxPawns;
    public int[] adjacentByLand;
    public int[] adjacentByWater;
    public int[] tokens = new int[]{0,0,0,0,0,0,0,0};
    public Player city = null;
    public int[] ships = new int[]{0,0,0,0,0,0,0,0};
    public int posPawnX;
    public int posPawnY;
    public int posCityX;
    public int posCityY;

    public int expansionCoeff = 0; //Is updated for each player, isn't a characteristic of the area

    //Constructors
    public Area(int num, boolean hasWater, boolean hasLand, boolean hasVolcano, boolean hasFloodPlain, boolean hasCitySite, boolean hasWhiteCitySite, int maxPawns, int[] adjacentByLand, int[] adjacentByWater, int posPawnX, int posPawnY, int posCityX, int posCityY) {
        this.num = num;
        this.hasWater = hasWater;
        this.hasLand = hasLand;
        this.hasVolcano = hasVolcano;
        this.hasFloodPlain = hasFloodPlain;
        this.hasCitySite = hasCitySite;
        this.hasWhiteCitySite = hasWhiteCitySite;
        this.maxPawns = maxPawns;
        this.adjacentByLand = adjacentByLand;
        this.adjacentByWater = adjacentByWater;
        this.posPawnX = posPawnX;
        this.posPawnY = posPawnY;
        this.posCityX = posCityX;
        this.posCityY = posCityY;
    }

    public Area(int num) {
        this.num = num;
    }

    protected Area(Parcel in) {
        num = in.readInt();
        hasWater = in.readByte() != 0x00;
        hasLand = in.readByte() != 0x00;
        hasVolcano = in.readByte() != 0x00;
        hasFloodPlain = in.readByte() != 0x00;
        hasCitySite = in.readByte() != 0x00;
        hasWhiteCitySite = in.readByte() != 0x00;
        maxPawns = in.readInt();
        city = (Player) in.readValue(Player.class.getClassLoader());
        posPawnX = in.readInt();
        posPawnY = in.readInt();
        posCityX = in.readInt();
        posCityY = in.readInt();
        expansionCoeff = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(num);
        dest.writeByte((byte) (hasWater ? 0x01 : 0x00));
        dest.writeByte((byte) (hasLand ? 0x01 : 0x00));
        dest.writeByte((byte) (hasVolcano ? 0x01 : 0x00));
        dest.writeByte((byte) (hasFloodPlain ? 0x01 : 0x00));
        dest.writeByte((byte) (hasCitySite ? 0x01 : 0x00));
        dest.writeByte((byte) (hasWhiteCitySite ? 0x01 : 0x00));
        dest.writeInt(maxPawns);
        dest.writeValue(city);
        dest.writeInt(posPawnX);
        dest.writeInt(posPawnY);
        dest.writeInt(posCityX);
        dest.writeInt(posCityY);
        dest.writeInt(expansionCoeff);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Area> CREATOR = new Parcelable.Creator<Area>() {
        @Override
        public Area createFromParcel(Parcel in) {
            return new Area(in);
        }

        @Override
        public Area[] newArray(int size) {
            return new Area[size];
        }
    };

}
