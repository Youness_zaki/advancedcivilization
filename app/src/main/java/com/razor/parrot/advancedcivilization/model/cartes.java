package com.razor.parrot.advancedcivilization.model;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import com.razor.parrot.advancedcivilization.R;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Stack;
import java.util.*;


public class cartes implements Parcelable {
    public String genre;
    public String name;
    public int value;
    public String trade;
    public int imgcarte;


    public cartes(String genre,String name, int value, String trade,int imgcarte){
        this.genre=genre;
        this.name=name;
        this.value=value;
        this.trade=trade;
        this.imgcarte=imgcarte;
    }

    protected cartes(Parcel in) {
        genre = in.readString();
        name = in.readString();
        value = in.readInt();
        trade = in.readString();
        imgcarte = in.readInt();

    }

    public static final Creator<cartes> CREATOR = new Creator<cartes>() {
        @Override
        public cartes createFromParcel(Parcel in) {
            return new cartes(in);
        }

        @Override
        public cartes[] newArray(int size) {
            return new cartes[size];
        }
    };

    public cartes() {
        this.genre=genre;
        this.name=name;
        this.value=value;
        this.trade=trade;
        this.imgcarte=imgcarte;

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(genre);
        parcel.writeString(name);
        parcel.writeInt(value);
        parcel.writeString(trade);
        parcel.writeInt(imgcarte);
    }
}


