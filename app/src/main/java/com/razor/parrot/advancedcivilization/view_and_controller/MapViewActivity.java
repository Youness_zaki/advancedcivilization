package com.razor.parrot.advancedcivilization.view_and_controller;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.opengl.Visibility;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Pair;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.razor.parrot.advancedcivilization.R;
import com.razor.parrot.advancedcivilization.model.Area;
import com.razor.parrot.advancedcivilization.model.Civilization;
import com.razor.parrot.advancedcivilization.model.Game;
import com.razor.parrot.advancedcivilization.model.Map;
import com.razor.parrot.advancedcivilization.model.Player;
import com.razor.parrot.advancedcivilization.model.cartes;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.*;
import java.util.Stack;
import java.util.Collections;

public class MapViewActivity extends AppCompatActivity {

    //Layout Objects
    ImageView imageViewMap;
    ImageView imageViewLayer;
    ImageView imageFlag;
    TextView info;
    TextView infoCase;
    TextView infoGame;
    Button okButton;
    Button cardsButton;
    Button skipButton;
    Bitmap mapBmp;
    Bitmap mapStartBmp;
    InputStream assetInStream2 = null;
    InputStream assetInStream3 = null;
    //For the refresh
    int w = 1595;
    int h = 1089;
    Bitmap.Config conf = Bitmap.Config.ARGB_8888; // see other conf types
    Bitmap layer = Bitmap.createBitmap(w, h, conf);
    Canvas canvas = new Canvas(layer); //New canvas for each refresh ?
    Paint paint = new Paint();

    //MODEL
    int nbPlayers;
    Map map;
    Game game;
    cartes cartes;
    Area activeArea;
    Player activePlayer;
    Player nullPlayer = new Player(0);
    ArrayList<Player> astOrder = new ArrayList<>(Arrays.asList (nullPlayer,nullPlayer,nullPlayer,nullPlayer,nullPlayer,nullPlayer,nullPlayer,nullPlayer,nullPlayer));


    //GAME
    static int[] phase = {0,0};
    ArrayList<Player> censusOrder = new ArrayList<>(nbPlayers);
    ArrayList<Player> stockOrder = new ArrayList<>(nbPlayers);
    int numNextPlayer;


    //PHASE 1
    ArrayList<Pair<Player,Integer>> revoltPlayers = new ArrayList<>(nbPlayers);
    int nbRevolts;
    Player bigStockPlayer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_map_view);

        //Initialisation
        nbPlayers = getIntent ().getIntExtra ("NbPlayers",0);
        map = new Map(this);
        game = new Game(this,nbPlayers, map);
        activeArea = map.areas[0];
        activePlayer = game.players[0];


        //RUSE
        try {
            assetInStream2 = getAssets().open("carte.png");
            assetInStream3 = getAssets().open ("carteStart.png");
        } catch (IOException e) {
            e.printStackTrace ( );
        }
        mapBmp = BitmapFactory.decodeStream (assetInStream2);
        mapStartBmp = BitmapFactory.decodeStream (assetInStream3);


        //Récupération des vues
        imageViewMap = (ImageView) findViewById (R.id.imageViewMap);
        info = (TextView) findViewById (R.id.textViewMap);
        infoCase = (TextView) findViewById (R.id.infoCase);
        infoGame = (TextView) findViewById (R.id.infoGame);
        okButton = (Button) findViewById (R.id.okButton);
        skipButton=(Button) findViewById(R.id.skipButton);
        cardsButton=(Button) findViewById (R.id.cardsButton);
        imageFlag = (ImageView) findViewById (R.id.imageViewFlag);
        imageViewLayer = (ImageView) findViewById (R.id.imageViewLayer);

        //
        imageViewLayer.setVisibility (View.VISIBLE);
        imageViewLayer.setImageBitmap (layer);
        imageViewMap.setImageBitmap (mapStartBmp);

        // test
        //info.setText (String.valueOf (nbPlayers));
        //info.setVisibility (info.VISIBLE);


        infoGame.setText ("Player " + activePlayer.num + " \n choose your civilization");
        infoGame.setVisibility (infoGame.VISIBLE);
        okButton.setVisibility (okButton.VISIBLE);
        final ArrayList<Civilization> listCivi = new ArrayList<> (Arrays.asList (game.africa, game.asia, game.assyria, game.babylon, game.crete, game.egypt, game.illyria, game.italy, game.thrace));



        imageViewMap.setOnTouchListener (new View.OnTouchListener ( ) {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        break;

                    case MotionEvent.ACTION_MOVE:
                        infoCase.setVisibility (infoCase.INVISIBLE);
                        break;

                    case MotionEvent.ACTION_UP:
                        //Getting the coordinates of the touch
                        float x = event.getX ( );
                        float y = event.getY ( );
                        //Getting the area touched
                        Area tempArea = map.whichArea (MapViewActivity.this, x, y);
                        if (tempArea.num != 0) {
                            activeArea = tempArea;
                            unselect();
                        }

                        //PHASE 0 : civilization choice
                        if (phase[0] == 0 && phase[1] == 1){
                            info.setText ("Player " + activePlayer.num + "\nArea " + activeArea.num);
                            info.setVisibility (info.VISIBLE);
                            boolean tempBool = false;
                            int j = 0;
                            while (j < listCivi.size ()){
                                if (listCivi.get(j).startingAreas.contains(activeArea)){
                                    tempBool = true;
                                    j = listCivi.size ();
                                }
                                else {
                                    j++;
                                }
                            }
                            if (tempBool){
                                select(activeArea);
                                okButton.setVisibility (okButton.VISIBLE);
                                okButton.setEnabled (true);
                            }
                            else {
                                okButton.setVisibility (okButton.INVISIBLE);
                                okButton.setEnabled (false);
                            }

                        }

                        //PHASE 1 : Tax collection  - revolts
                        else if (phase[0] == 1 && phase[1] == 4){
                            if (activePlayer.cities.contains (activeArea)){
                                select(activeArea);
                                okButton.setVisibility (okButton.VISIBLE);
                                okButton.setEnabled (true);
                            }
                            else {
                                okButton.setVisibility (okButton.INVISIBLE);
                                okButton.setEnabled (false);
                            }
                        }

                        //PHASE 2 : Population expansion - not enough stock
                        else if (phase[0]== 2 && phase[1] == 3){
                            if (activeArea.expansionCoeff > 0){
                                select (activeArea);
                                addToken(activePlayer,activeArea);
                                activeArea.expansionCoeff --;
                                if (activePlayer.stock == 0){
                                    phase[1]= 4;
                                    okButton.setEnabled(true);
                                    okButton.setVisibility (View.VISIBLE);
                                }
                            }
                        }

                        //PHASE 4 : Ship construction - maintenance
                        else if (phase[0] == 4 && phase[1] == 3){
                            maintainShip(activePlayer,activeArea);
                            phase[1]=4;

                        }

                        else if (phase[0]==4 && phase[1]==5) {
                            infoGame.setVisibility(View.INVISIBLE);
                            okButton.setVisibility(View.INVISIBLE);
                            info.setText (activePlayer.civilization.name + "\nArea " + activeArea.num);
                            info.setVisibility (info.VISIBLE);
                            //activePlayer.ships.contains(activeArea)
                            if (map.listWater[activeArea.num]==true && !activePlayer.ships.contains(activeArea)){
                                select(activeArea);
                                okButton.setVisibility(okButton.VISIBLE);
                                okButton.setEnabled(true);
                            }
                            else{
                                okButton.setVisibility (okButton.INVISIBLE);
                                okButton.setEnabled (false);
                            }

                        }

                        //PHASE 5: city construction
                        else if (phase[0] == 5 && phase[1] == 3) {
                            infoGame.setVisibility(View.INVISIBLE);
                            okButton.setVisibility(View.INVISIBLE);
                            info.setText (activePlayer.civilization.name + "\nArea " + activeArea.num);
                            info.setVisibility (info.VISIBLE);
                            if (map.citySites.contains(activeArea.num) || (map.whiteCitySites.contains(activeArea.num))){
                                select(activeArea);
                                okButton.setVisibility(okButton.VISIBLE);
                                okButton.setEnabled(true);
                            }
                            else{
                                okButton.setEnabled(false);
                                okButton.setVisibility(okButton.INVISIBLE);
                            }

//                            int j;
//                            for(j=0;j<map.citySites.size();j++){
//                                if (map.citySites.get(j)==activeArea.num){
//                                    select(activeArea);
//                                    addCity(activePlayer,activeArea);
//                                    okButton.setVisibility(okButton.VISIBLE);
//                                    okButton.setEnabled(true);
//                                }
//                                else{
//                                    j++;
//                                }
//                            }


                        }

                        else if (phase[0]==5 && phase[1]==4){
                            okButton.setVisibility(okButton.INVISIBLE);
                            okButton.setEnabled(false);
                        }




                }

                return true;
            }
        });

        cardsButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MapViewActivity.this, MainCartesActivity.class);
                intent.putExtra ("NbPlayers",nbPlayers);
                intent.putExtra("Game",game);
                intent.putExtra("cartes",cartes);
                intent.putExtra("Area",activeArea);
                intent.putExtra("Player",activePlayer);
                intent.putParcelableArrayListExtra("astOrder",astOrder);
                startActivity (intent);
            }
        });

        skipButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                if (phase[0]==5 && phase[1]==2){
                    if (numNextPlayer < nbPlayers){
                        activePlayer = astOrder.get(numNextPlayer);
                        numNextPlayer ++;
                        phase[1]=1;
                    }
                    //The phase is complete, every player has built a city
                    else {
                        phase[0] = 6;
                        phase[1]=0;
                    }
                }
            }
        });



        okButton.setOnClickListener (new View.OnClickListener ( ) {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {

                unselect ();
                //PHASE 0 : Civilization choice
                if (phase[0] == 0 && phase[1] == 0){
                    infoGame.setVisibility (infoGame.INVISIBLE);
                    okButton.setVisibility (okButton.INVISIBLE);
                    okButton.setEnabled (false);
                    phase[1] = 1;
                }

                else if (phase[0] == 0 && phase[1] == 1){
                    info.setVisibility (View.INVISIBLE);

                    //Test for each civilization
                    if (game.africa.startingAreas.contains (activeArea)){
                        activePlayer.civilization = game.africa;
                        listCivi.remove(game.africa);
                        infoGame.setText ("Player " + activePlayer.num + " \n You have chosen Africa");
                        infoGame.setVisibility (infoGame.VISIBLE);
                        astOrder.set(0, activePlayer);
                    }
                    else if (game.asia.startingAreas.contains (activeArea)){
                        activePlayer.civilization = game.asia;
                        listCivi.remove(game.asia);
                        infoGame.setText ("Player " + activePlayer.num + " \n You have chosen Asia");
                        infoGame.setVisibility (infoGame.VISIBLE);
                        astOrder.set(5, activePlayer);
                    }
                    else if (game.assyria.startingAreas.contains (activeArea)){
                        activePlayer.civilization = game.assyria;
                        listCivi.remove(game.assyria);
                        infoGame.setText ("Player " + activePlayer.num + " \n You have chosen Assyria");
                        infoGame.setVisibility (infoGame.VISIBLE);
                        astOrder.set(6, activePlayer);
                    }
                    else if (game.babylon.startingAreas.contains (activeArea)){
                        activePlayer.civilization = game.babylon;
                        listCivi.remove(game.babylon);
                        infoGame.setText ("Player " + activePlayer.num + " \n You have chosen Babylon");
                        infoGame.setVisibility (infoGame.VISIBLE);
                        astOrder.set(7, activePlayer);
                    }
                    else if (game.crete.startingAreas.contains (activeArea)){
                        activePlayer.civilization = game.crete;
                        listCivi.remove(game.crete);
                        infoGame.setText ("Player " + activePlayer.num + " \n You have chosen Crete");
                        infoGame.setVisibility (infoGame.VISIBLE);
                        astOrder.set(4, activePlayer);
                    }
                    else if (game.egypt.startingAreas.contains (activeArea)){
                        activePlayer.civilization = game.egypt;
                        listCivi.remove(game.egypt);
                        infoGame.setText ("Player " + activePlayer.num + " \n You have chosen Egypt");
                        infoGame.setVisibility (infoGame.VISIBLE);
                        astOrder.set(8, activePlayer);
                    }
                    else if (game.illyria.startingAreas.contains (activeArea)){
                        activePlayer.civilization = game.illyria;
                        listCivi.remove(game.illyria);
                        infoGame.setText ("Player " + activePlayer.num + " \n You have chosen Illyria");
                        infoGame.setVisibility (infoGame.VISIBLE);
                        astOrder.set(2, activePlayer);
                    }
                    else if (game.italy.startingAreas.contains (activeArea)){
                        activePlayer.civilization = game.italy;
                        listCivi.remove(game.italy);
                        infoGame.setText ("Player " + activePlayer.num + " \n You have chosen Italy");
                        infoGame.setVisibility (infoGame.VISIBLE);
                        astOrder.set(1, activePlayer);
                    }
                    else if (game.thrace.startingAreas.contains (activeArea)){
                        activePlayer.civilization = game.thrace;
                        listCivi.remove(game.thrace);
                        infoGame.setText ("Player " + activePlayer.num + " \n You have chosen Thrace");
                        infoGame.setVisibility (infoGame.VISIBLE);
                        astOrder.set(3, activePlayer);
                    }

                    addToken (activePlayer,activeArea);
                    refreshMap ();

                    censusOrder.add(activePlayer);
                    stockOrder.add(activePlayer);

                    okButton.setVisibility (okButton.VISIBLE);
                    okButton.setEnabled (true);

                    phase[1] = 2;

                }

                else if (phase[0] == 0 && phase[1] == 2){
                    numNextPlayer = activePlayer.num + 1;
                    //If every player has chosen a civilization
                    if (numNextPlayer == nbPlayers + 1){
                        //One or more civilizations are not chosen, get rid of them in the AST order
                        while (astOrder.size () != nbPlayers){
                            astOrder.remove (nullPlayer);
                        }

                        infoGame.setText ("Each player corresponds \nnow to a civilization");
                        imageViewMap.setImageBitmap (mapBmp);
                        infoGame.setVisibility (infoGame.VISIBLE);
                        //phase suivante
                        phase[0] = 1;
                        activePlayer = astOrder.get(0);
                    }
                    //If every player has not chosen yet a civilization
                    else {
                        activePlayer = game.players[numNextPlayer - 1];
                        infoGame.setText ("Player " + activePlayer.num + " \nchoose your civilization");
                        infoGame.setVisibility (infoGame.VISIBLE);
                    }
                    phase[1] = 0;
                }


                //PHASE 1 : Tax collection
                else if (phase[0] == 1 && phase[1] == 0){
                    //HIJACKING for test
                    //map.areas[3].city = game.players[0];
                    //game.players[0].cities.add (map.areas[3]);
                    //map.areas[6].city = game.players[0];
                    //game.players[0].cities.add (map.areas[6]);
                    //map.areas[2].city = game.players[0];
                    //game.players[0].cities.add (map.areas[2]);
//                    game.players[0].stock = 5;
//                    map.areas[19].city = game.players[1];
//                    game.players[1].cities.add(map.areas[19]);
//                    refreshMap ();



                    okButton.setEnabled (false);
                    infoGame.setText ("Tax collection : Each player must \npay 2 for each of their city");
                    infoGame.setVisibility (infoGame.VISIBLE);
                    //PHASE1 : Collect Taxes
                    for(Player tempPlayer : astOrder){
                        int nbCities = tempPlayer.cities.size ();
                        //If the player can pay for every city
                        if(tempPlayer.stock >= nbCities*2){
                            tempPlayer.stock -= nbCities*2;
                            tempPlayer.treasury += nbCities*2;
                        }
                        //If not
                        else{
                            int nbCitiesPaid = tempPlayer.stock/2;
                            tempPlayer.stock -= nbCitiesPaid*2;
                            tempPlayer.treasury += nbCitiesPaid*2;
                            revoltPlayers.add(new Pair<> (tempPlayer,nbCities - nbCitiesPaid));
                        }
                    }

                    sortStock();

                    phase[1] = 1;
                    okButton.setEnabled (true);
                }

                else if (phase[0] == 1 && phase[1] == 1){
                    okButton.setEnabled(false);
                    if (revoltPlayers.size () > 0){
                        activePlayer = revoltPlayers.get(0).first;
                        nbRevolts = revoltPlayers.get(0).second;
                        revoltPlayers.remove (0);
                        infoGame.setText ("Tax collection : " + activePlayer.civilization.name + ",\nyou have " + nbRevolts + " city-ies revolting");
                        infoGame.setVisibility (View.VISIBLE);
                        phase[1] = 2;
                    }
                    else {
                        infoGame.setText ("Tax collection done");
                        infoGame.setVisibility (View.VISIBLE);
                        phase[0] = 2;
                        phase[1] = 0;
                        activePlayer = astOrder.get(0);
                    }
                    okButton.setEnabled(true);
                }

                else if (phase[0] == 1 && phase[1] == 2){
                    //Revolt
                    //Get the player with the biggest stock who's not the active player
                    bigStockPlayer = stockOrder.get(nbPlayers-1);
                    int j = 2;
                    while (bigStockPlayer == activePlayer || bigStockPlayer.cityPawns == 0){
                        bigStockPlayer = stockOrder.get (nbPlayers-j);
                        j++;
                    }

                    infoGame.setText ("Tax collection : " + bigStockPlayer.civilization.name + ",\n choose " + nbRevolts + " city-ies from " + activePlayer.civilization.name + " to revolt");
                    phase[1] = 3;
                }

                else if (phase[0] == 1 && phase[1] == 3){
                    infoGame.setVisibility (View.INVISIBLE);
                    okButton.setVisibility (View.INVISIBLE);
                    okButton.setEnabled (false);
                    phase[1] = 4;
                }

                else if (phase[0] == 1 && phase[1] == 4){
                    if (bigStockPlayer.cityPawns > 0){
                        okButton.setEnabled(false);
                        bigStockPlayer.cityPawns--;
                        activeArea.city = bigStockPlayer;
                        bigStockPlayer.cities.add(activeArea);
                        activePlayer.cities.remove(activeArea);
                        nbRevolts--;
                        infoGame.setText ("Tax collection : " + bigStockPlayer.civilization.name + "\n you have taken over this city");
                        infoGame.setVisibility (View.VISIBLE);
                        refreshMap ();
                        if (nbRevolts > 0){
                            phase[1] = 3;
                        }
                        else {
                            phase[1] = 1;
                        }

                        okButton.setEnabled(true);
                    }
                    else {
                        infoGame.setText ("Tax collection : " + bigStockPlayer.civilization.name + "\n you have no city pawn left");
                        infoGame.setVisibility (View.VISIBLE);
                        phase[1] = 2;
                    }
                }


                //PHASE 2 : Population expansion
                else if (phase[0]==2 && phase[1]==0){
                    okButton.setEnabled (false);

                    //HIJACKING
                   /* game.players[0].stock = 10;
                    map.areas[9].tokens[0] = 2;
                    map.areas[10].tokens[0]= 1;
                    game.players[1].stock = 3;
                    map.areas[5].tokens[1] = 2;
                    map.areas[4].tokens[1] = 2;*/


                    infoGame.setText("Expansion phase: \nTime for demographic growth");
                    infoGame.setVisibility(View.VISIBLE);
                    activePlayer = astOrder.get (0);
                    numNextPlayer = 1; //in the AST order
                    phase[1] = 1;
                    okButton.setVisibility(View.VISIBLE);
                    okButton.setEnabled(true);
                }

                else if (phase[0]==2 && phase[1]==1){
                    okButton.setEnabled (false);

                    int expansionCount = 0;
                    for (Area area: map.areas){
                        if (area.city == null){
                            if (area.tokens[activePlayer.num - 1] == 1){
                                expansionCount ++;
                            }
                            else if (area.tokens[activePlayer.num - 1] > 1){
                                expansionCount += 2;
                            }
                        }
                    }
                    //The player has enough stock for population growth everywhere
                    if (expansionCount <= activePlayer.stock){
                        //Population growth
                        for (Area area: map.areas){
                            if (area.city == null){
                                if (area.tokens[activePlayer.num - 1] == 1){
                                    addToken(activePlayer, area);
                                }
                                else if (area.tokens[activePlayer.num - 1] > 1){
                                    addToken(activePlayer, area);
                                    addToken(activePlayer, area);
                                }
                            }
                        }

                        infoGame.setText(activePlayer.civilization.name + ":\nExpansion complete");
                        infoGame.setVisibility(View.VISIBLE);

                        //All the players haven't expanded
                        if (numNextPlayer < nbPlayers){
                            activePlayer = astOrder.get(numNextPlayer);
                            numNextPlayer ++;
                        }
                        //The phase is complete, every player has expanded
                        else {
                            phase[1] = 5;
                        }
                    }
                    //The player has no stock left
                    else if (activePlayer.stock == 0){
                        infoGame.setText(activePlayer.civilization.name+":\nNo stock left to expand");
                        infoGame.setVisibility(View.VISIBLE);

                        //All the players haven't expanded
                        if (numNextPlayer < nbPlayers){
                            activePlayer = astOrder.get(numNextPlayer);
                            numNextPlayer ++;
                        }
                        //The phase is complete, every player has expanded
                        else {
                            phase[1] = 5;
                        }
                    }
                    //The player doesn't have enough stock to complete the population growth
                    else {
                        infoGame.setText(activePlayer.civilization.name + ": You don't have enough stock \nto expand your civilization completely");
                        infoGame.setVisibility(View.VISIBLE);
                        phase[1] = 2;
                    }

                    okButton.setEnabled (true);

                }

                else if (phase[0] == 2 && phase[1] == 2){
                    okButton.setEnabled (false);
                    infoGame.setText("Please choose the areas in which \nyou want to spend your stock");
                    infoGame.setVisibility(View.VISIBLE);
                    //On définit pour chaque case un coefficient d'expansion maximale qui permettra de limiter les actions du joueur
                    for (Area area: map.areas){
                        if (area.tokens[activePlayer.num - 1] == 1){
                            area.expansionCoeff = 1;
                        }
                        else if (area.tokens[activePlayer.num - 1] > 1){
                            area.expansionCoeff = 2;
                        }
                    }
                    phase[1] = 3;
                    okButton.setEnabled(true);
                }

                else if (phase[0] == 2 && phase[1] == 3){
                    infoGame.setVisibility (View.INVISIBLE);
                    okButton.setEnabled (false);
                    okButton.setVisibility (View.INVISIBLE);
                }

                else if (phase[0] == 2 && phase[1] == 4){
                    okButton.setEnabled (false);
                    infoGame.setText("Expansion complete for "+activePlayer.civilization.name);
                    infoGame.setVisibility(View.VISIBLE);

                    if (numNextPlayer < nbPlayers){
                        phase[1]= 1;
                        activePlayer = astOrder.get(numNextPlayer);
                        numNextPlayer ++;
                    }
                    else{
                        phase[1] = 5;
                    }
                    okButton.setEnabled(true);
                }

                else if (phase[0] == 2 && phase[1] == 5){
                    infoGame.setText("Expansion phase complete");
                    infoGame.setVisibility(View.VISIBLE);
                    phase[0]=3;
                    phase[1]=0;
                }

                //PHASE 3 : Census
                else if (phase[0] == 3 && phase[1] == 0){
                    okButton.setEnabled (false);
                    //Census
                    for (Player player : game.players){
                        player.census = 55 - player.stock - player.treasury;
                    }
                    sortCensus();
                    infoGame.setText("Census done");
                    infoGame.setVisibility(View.VISIBLE);
                    activePlayer = censusOrder.get(nbPlayers - 1);
                    numNextPlayer = nbPlayers - 2; //Decreasing this time because census order is crescent
                    phase[0] = 4;
                    okButton.setEnabled (true);
                }

                //PHASE 4 : Ship construction
                else if (phase[0] == 4 && phase[1] == 0){
                    activePlayer = astOrder.get (0);
                    numNextPlayer = 1;
                    infoGame.setText ("Ship construction : building costs 2 tokens\n from stock and/or from the area in which\nyou wish to place it\nShip maintenance costs 1 token");
                    infoGame.setVisibility(View.VISIBLE);
                    phase[1] = 1;
                }

                else if (phase[0] == 4 && phase[1] == 1){
                    okButton.setEnabled (false);
                    infoGame.setText (activePlayer.civilization.name + ":\n your turn to build/maintain ships");
                    if (activePlayer.ships.size () != 0){
                        phase[1] = 2;
                    }
                    else {
                        phase[1] = 4;
                    }
                    okButton.setEnabled (true);
                }

                else if (phase[0] == 4 && phase[1] == 2){
                    infoGame.setText (activePlayer.civilization.name + ":\n Choose the ships you wish to maintain");
                    okButton.setEnabled(false);
                    phase[1] = 3;
                }

                else if (phase[0] == 4 && phase[1] == 3){
                    infoGame.setVisibility (View.INVISIBLE);
                    okButton.setEnabled (true);
                }

                else if (phase[0]==4 && phase[1]==4){
                    infoGame.setText(activePlayer.civilization.name + ":\n Choose the area in which you want to build a ship");
                    okButton.setEnabled(false);
                    phase[1]=5;
                }

                else if (phase[0]==4 && phase[1]==5){
                    addShip(activePlayer,activeArea);
                    infoGame.setText("You have build a ship");
                    infoGame.setVisibility(View.VISIBLE);
                    okButton.setEnabled(true);
                    if (numNextPlayer < nbPlayers){
                        activePlayer = astOrder.get(numNextPlayer);
                        numNextPlayer ++;
                        phase[1]=1;
                    }
                    //The phase is complete, every player has built/maintain ships
                    else {
                        phase[0] = 5;
                        phase[1]=0;
                    }
                }
                //PHASE 5: City construction
                else if (phase[0]==5 && phase[1]==0){
                    activePlayer = astOrder.get (0);
                    numNextPlayer = 1;
                    infoGame.setText("city construction: building costs 6 tokens from the area in which\n you wish to place it");
                    infoGame.setVisibility(View.VISIBLE);
                    okButton.setEnabled(true);
                    phase[1]=1;
                }

                else if (phase[0]==5 && phase [1]==1){
                    infoGame.setText(activePlayer.civilization.name + ":\n your turn to build cities");
                    if (activePlayer.cityPawns!=0){
                        phase[1]=2;

                    }
                    else{
                        phase[1]=4;
                    }
                    //okButton.setEnabled(true);
                    skipButton.setVisibility(skipButton.VISIBLE);
                    skipButton.setEnabled(true);

                }

                else if (phase[0]==5 && phase[1]==2){
                    infoGame.setText(activePlayer.civilization.name +  ":\n Choose the area in which you wish to build a city");
                    okButton.setEnabled(false);
                    phase[1]=3;
                }


                else if (phase[0]==5 && phase[1]==3){
                    addCity(activePlayer,activeArea);
                    infoGame.setText("You have built a city here ");
                    infoGame.setVisibility(View.VISIBLE);
                    okButton.setEnabled(true);
                    if (numNextPlayer < nbPlayers){
                        activePlayer = astOrder.get(numNextPlayer);
                        numNextPlayer ++;
                        phase[1]=1;
                    }
                    //The phase is complete, every player has built a city
                    else {
                        phase[0] = 6;
                        phase[1]=0;
                    }
                }

                //PHASE 6: Distribution des cartes
                else if (phase[0]==6 && phase[1]==0){
                    activePlayer = astOrder.get (0);
                    numNextPlayer = 1;
                    infoGame.setText("commerce cards distribution");
                    infoGame.setVisibility(View.VISIBLE);

                    phase[1]=1;
                    okButton.setVisibility(okButton.VISIBLE);
                    okButton.setEnabled(true);
                }

                else if (phase[0]==6 && phase [1]==1){
                    infoGame.setText(activePlayer.civilization.name +":\n You will receive " + activePlayer.cities.size() + " commerce card");
                    infoGame.setVisibility(View.VISIBLE);
                    cardsdistribution(activePlayer);
                    phase[1]=2;
                    okButton.setVisibility(okButton.VISIBLE);
                    okButton.setEnabled(true);

                }

                else if (phase[0]==6 && phase[1]==2){
                    //infoGame.setText(activePlayer.civilization.name +":\n You will receive " + activePlayer.cities.size() + " commerce card");
                    //infoGame.setVisibility(View.VISIBLE);
                    if (numNextPlayer < nbPlayers){
                        activePlayer = astOrder.get(numNextPlayer);
                        numNextPlayer ++;
                        phase[1]=1;
                    }
                    //The phase is complete, every player has built/maintain ships
                    else { ;
                        phase[1]=3;
                        infoGame.setText("Commerce cards distribution has been done");
                        infoGame.setVisibility(View.VISIBLE);
                    }
                    okButton.setVisibility(okButton.VISIBLE);
                    okButton.setEnabled(true);
                }


                else if (phase[0]==6 && phase[1]==3){
                    Intent intent = new Intent(MapViewActivity.this, MainCartesActivity.class);
                    intent.putExtra ("NbPlayers",nbPlayers);
                    intent.putExtra("Game",game);
                    intent.putExtra("cartes",cartes);
                    intent.putExtra("Area",activeArea);
                    intent.putExtra("Player",activePlayer);
                    startActivity (intent);

                }


            }
        });


    }




    //METHODS

    public void sortStock(){
        //Insertion Sort (rising order)
        for (int i = 1; i < nbPlayers; i++){
            Player tempPlayer = stockOrder.get(i);
            int j = i;
            while (j > 0 && stockOrder.get (j-1).stock > tempPlayer.stock){
                j--;
            }
            stockOrder.remove (i);
            stockOrder.add(j,tempPlayer);
        }
    }

    public void sortCensus(){
        //Insertion Sort (rising order)
        for (int i = 1; i < nbPlayers; i++){
            Player tempPlayer = censusOrder.get(i);
            int j = i;
            while (j > 0 && censusOrder.get (j-1).census > tempPlayer.census){
                j--;
            }
            censusOrder.remove (i);
            censusOrder.add(j,tempPlayer);
        }
    }

    public void select(Area area){
        float density = getResources ( ).getDisplayMetrics ( ).density;
        imageFlag.setX ((area.posPawnX-90*166/460)*density);
        imageFlag.setY ((area.posPawnY-130)*density);
        imageFlag.setVisibility (View.VISIBLE);
    }

    public void unselect(){
            imageFlag.setVisibility (View.INVISIBLE);
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public void addToken(Player player, Area area){
        area.tokens[player.num - 1] ++;
        player.stock --;
        player.census ++;
        refreshMap();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void addCity(Player player, Area area) {
                    player.cities.add(area);
                    //area.tokens[player.num] = -6;
                    refreshMap();
        }



//    public boolean addCity(Player player, Area area) {
//        boolean addCity=false;
//            if ((player.cities.contains(area))||( area.city!=null)) {
//            }
//            else {
//                if ((area.hasCitySite = true) || (area.hasWhiteCitySite = true)) {
//                    if (area.tokens[player.num] >= 6 && player.stock >= 2) {
//                        player.cities.add(area);
//                        area.tokens[player.num] = -6;
//                        addCity=true;
//                    }
//                }
//                else {
//                    if (area.tokens[player.num] >= 12 && player.stock >= 2) {
//                        player.cities.add(area);
//                        area.tokens[player.num] = -12;
//                        addCity=true;
//                    }
//                }
//
//            }
//        return addCity;
//    }

//    public void addShip(Player player, Area area){
//        boolean addship =false;
//        if (player.ships.contains(area)){
//        }
//        else{
//            if (area.hasWater = true){
//                if ((area.tokens[player.num -1] >= 2) ) {
//                    player.ships.add(area);
//                    area.tokens[player.num -1] -= 2;
//                    addship = true;
//                }
//                else if (player.stock>=2) {
//                    player.ships.add(area);
//                    player.stock -= 2;
//                    addship = true;
//                }
//                else if (player.stock==1 && area.tokens[player.num -1]==1){
//                    player.ships.add(area);
//                    player.stock -= 1;
//                    area.tokens[player.num] -= 1;
//                    addship= true;
//
//                }
//
//            }
//        }
//        return addship;
//
//    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void addShip(Player player, Area area){
        if ((area.tokens[player.num -1] >= 2) ) {
            player.ships.add(area);
            area.tokens[player.num -1] -= 2;
            refreshMap();
        }

        else if (player.stock>=2) {
            player.ships.add(area);
            player.stock -= 2;
            refreshMap();
        }

        else if (player.stock==1 && area.tokens[player.num -1]==1){
            player.ships.add(area);
            player.stock -= 1;
            area.tokens[player.num] -= 1;
            refreshMap();
        }
    }



    public void maintainShip (Player player, Area area){
        if (player.treasury>0){
            player.treasury-=1;
        }
        else if (area.tokens[player.num]>0){
            area.tokens[player.num]-=1;
        }
        else{
            player.ships.remove(area);
            player.shipPawns+=1;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void cardsdistribution(Player player){

        if (player.cities.size()==1){
            player.cartesdujoueur.add(game.premierepiledecartes.pop());
        }

        else if (player.cities.size()==2){
            player.cartesdujoueur.add(game.premierepiledecartes.pop());
            player.cartesdujoueur.add(game.deuxiemepiledecartes.pop());

        }

        else if (player.cities.size()==3){
            player.cartesdujoueur.add(game.premierepiledecartes.pop());
            player.cartesdujoueur.add(game.deuxiemepiledecartes.pop());
            player.cartesdujoueur.add(game.troisiemepiledecartes.pop());

        }

        else if (player.cities.size()==4){
            player.cartesdujoueur.add(game.premierepiledecartes.pop());
            player.cartesdujoueur.add(game.deuxiemepiledecartes.pop());
            player.cartesdujoueur.add(game.troisiemepiledecartes.pop());
            player.cartesdujoueur.add(game.quatriemepiledecartes.pop());

        }

        else if (player.cities.size()==5){
            player.cartesdujoueur.add(game.premierepiledecartes.pop());
            player.cartesdujoueur.add(game.deuxiemepiledecartes.pop());
            player.cartesdujoueur.add(game.troisiemepiledecartes.pop());
            player.cartesdujoueur.add(game.quatriemepiledecartes.pop());
            player.cartesdujoueur.add(game.cinquiemepiledecartes.pop());

        }

        else if (player.cities.size()==6){
            player.cartesdujoueur.add(game.premierepiledecartes.pop());
            player.cartesdujoueur.add(game.deuxiemepiledecartes.pop());
            player.cartesdujoueur.add(game.troisiemepiledecartes.pop());
            player.cartesdujoueur.add(game.quatriemepiledecartes.pop());
            player.cartesdujoueur.add(game.cinquiemepiledecartes.pop());
            player.cartesdujoueur.add(game.sixiemepiledecartes.pop());
        }

        else if (player.cities.size()==7){
            player.cartesdujoueur.add(game.premierepiledecartes.pop());
            player.cartesdujoueur.add(game.deuxiemepiledecartes.pop());
            player.cartesdujoueur.add(game.troisiemepiledecartes.pop());
            player.cartesdujoueur.add(game.quatriemepiledecartes.pop());
            player.cartesdujoueur.add(game.cinquiemepiledecartes.pop());
            player.cartesdujoueur.add(game.sixiemepiledecartes.pop());
            player.cartesdujoueur.add(game.septiemepiledecartes.pop());

        }

        else if (player.cities.size()==8){
            player.cartesdujoueur.add(game.premierepiledecartes.pop());
            player.cartesdujoueur.add(game.deuxiemepiledecartes.pop());
            player.cartesdujoueur.add(game.troisiemepiledecartes.pop());
            player.cartesdujoueur.add(game.quatriemepiledecartes.pop());
            player.cartesdujoueur.add(game.cinquiemepiledecartes.pop());
            player.cartesdujoueur.add(game.sixiemepiledecartes.pop());
            player.cartesdujoueur.add(game.septiemepiledecartes.pop());
            player.cartesdujoueur.add(game.huitiemepiledecartes.pop());

        }

        else if (player.cities.size()==9){
            player.cartesdujoueur.add(game.premierepiledecartes.pop());
            player.cartesdujoueur.add(game.deuxiemepiledecartes.pop());
            player.cartesdujoueur.add(game.troisiemepiledecartes.pop());
            player.cartesdujoueur.add(game.quatriemepiledecartes.pop());
            player.cartesdujoueur.add(game.cinquiemepiledecartes.pop());
            player.cartesdujoueur.add(game.sixiemepiledecartes.pop());
            player.cartesdujoueur.add(game.septiemepiledecartes.pop());
            player.cartesdujoueur.add(game.huitiemepiledecartes.pop());
            player.cartesdujoueur.add(game.neuviemepiledecartes.pop());

        }
    }




    @RequiresApi(api = Build.VERSION_CODES.O)
    public void refreshMap() { //ON a un probleme de save et restore
        //canvas.restore();
        //canvas.save();
        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        for (Area area : map.areas) {
            for (Player player : game.players) {
                //Tokens display
                if (area.tokens[player.num - 1] > 0) {
                    Bitmap pawnBmp = player.civilization.pawnImg;
                    int left = area.posPawnX - (player.num) * pawnBmp.getWidth();
                    int top = area.posPawnY + pawnBmp.getHeight() / 2;
                    canvas.drawBitmap(pawnBmp, left, top, null);
                    paint.setARGB(255, 255, 0, 0);
                    canvas.drawCircle((float) left + pawnBmp.getWidth(), (float) top + pawnBmp.getHeight(), 5, paint);
                    paint.setARGB(255, 255, 255, 255);
                    canvas.drawText(String.valueOf(area.tokens[player.num - 1]), (float) left + pawnBmp.getWidth() - 2, (float) top + pawnBmp.getHeight() + 2, paint);
                }

                if (player.ships.contains(area)) {
                    Bitmap pawnBmp = player.civilization.pawnImg;
                    int left = area.posPawnX - (player.num) * pawnBmp.getWidth();
                    int top = area.posPawnY + pawnBmp.getHeight() / 2;
                    canvas.drawBitmap(pawnBmp, left, top, null);
                    //paint.setARGB(255, 255, 0, 0);
                    //canvas.drawCircle((float) left + pawnBmp.getWidth(), (float) top + pawnBmp.getHeight(), 5, paint);
                    //paint.setARGB(255, 255, 255, 255);
                    canvas.drawText(String.valueOf(area.tokens[player.num - 1]), (float) left + pawnBmp.getWidth() - 2, (float) top + pawnBmp.getHeight() + 2, paint);
                }

                if (player.cities.contains(area)) {
                    Bitmap cityBmp = player.civilization.cityImg;
                    int left = area.posCityX - (player.num) * cityBmp.getWidth();
                    int top = area.posPawnY + cityBmp.getHeight() / 2;
                    canvas.drawBitmap(cityBmp, left, top, null);
                    //paint.setARGB(255, 255, 0, 0);
                    //canvas.drawCircle((float) left + cityBmp.getWidth(), (float) top + cityBmp.getHeight(), 5, paint);
                    //paint.setARGB(255, 255, 255, 255);
                    canvas.drawText (player.civilization.name, (float) left, (float) top - 5, paint);

                    //canvas.drawText(String.valueOf(area.tokens[player.num - 1]), (float) left + cityBmp.getWidth() - 2, (float) top + cityBmp.getHeight() + 2, paint);

                }
                            //Cities display
//                if (player.cities.contains (area)){
//                    paint.setARGB (Color.alpha (player.civilization.color), Color.red (player.civilization.color), Color.green(player.civilization.color), Color.blue(player.civilization.color));
//                    int left = area.posCityX;
//                    int top = area.posCityY;
//                    int right = left + 9;
//                    int bottom = top + 9;
//                    canvas.drawRect ((float) left, (float) top, (float) right,(float) bottom, paint);
//                    canvas.drawText (player.civilization.name, (float) left, (float) top - 5, paint);
//
//                    Bitmap cityPawnBmp = player.civilization.cityImg;
//                    canvas.drawBitmap(cityPawnBmp, left, top, null);
//                }
            }
        }
        //this.imageViewLayer.setImageBitmap(layer);

    }


}