package com.razor.parrot.advancedcivilization.model;

import com.razor.parrot.advancedcivilization.R;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Parcelable;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class Map {

    private static Bitmap colorImg = null;
    private static Bitmap mapImg = null;
    public Area[] areas = new Area[131];
    public ArrayList<Integer> getCitySites;
    public boolean [] getListWater;
    public boolean [] getListLand;
    InputStream assetInStream = null;

    //BDD
    public final boolean[] listWater = {false /*case0*/,
            true, true, true, true, true, true, true, true, true, true,
            true, true, true, true, true, true, true, true, true, true,
            true, false, false, false, true, true, false, false, true, false,
            false, false, true, true, true, true, true, true, true, true,
            true, true, true, true, true, true, false, true, true, false,
            true, true, true, true, true, true, true, true, true, true,
            true, true, true, true, true, true, true, true, true, true,
            true, true, true, true, false, true, false, false, false, true,
            true, true, true, true, false, true, true, false, true, true,
            true, true, true, true, true, true, true, false, false, false,
            true, true, false, true, true, true, true, true, true, true,
            false, true, true, true, false, false, true, true, true, false,
            false, false, false, true, true, false, true, true, true, false,
    };

    private final boolean[] listLand = {false /*case0*/,
            false, true, true, true, true, true, true, true, true, true,
            true, true, true, true, false, false, true, true, true, true,
            true, true, true, true, true, true, true, true, true, true,
            true, true, true, true, true, true, true, true, true, false,
            true, true, true, true, true, true, true, true, true, true,
            true, true, true, true, true, true, true, true, true, true,
            true, true, true, true, true, true, true, true, true, true,
            true, true, false, true, true, true, true, true, true, true,
            true, true, true, true, true, true, true, true, true, true,
            true, true, true, true, true, true, true, true, true, true,
            true, true, true, true, true, true, true, true, true, true,
            true, true, true, true, true, true, true, false, true, true,
            true, true, true, true, true, true, true, true, true, true,
    };

    private final int[][] adjacencyByLand = {{} /*case0*/,
            {}, {3, 4}, {4, 2}, {3, 2}, {7, 8, 6}, {8, 7, 5}, {9, 10, 8, 5}, {10, 9, 7, 5, 6}, {11, 10, 8, 7}, {8, 9, 11, 28, 29},
            {28, 10, 9}, {}, {14}, {13}, {}, {}, {18, 19}, {17, 19, 22}, {17, 18, 22, 20, 21}, {19, 22, 23, 24, 21},
            {20, 24, 25}, {18, 19, 20, 23}, {22, 20, 24}, {23, 20, 21, 25, 27}, {21, 24, 27, 26}, {25, 27, 74}, {24, 25, 26, 74, 75}, {29, 10, 11}, {10, 28, 30, 34, 33}, {29, 34, 32, 31},
            {30, 32, 35}, {30, 31, 35, 36, 42, 34}, {29, 34}, {33, 29, 30, 32, 42, 47, 49}, {31, 32, 36, 38}, {32, 35, 41, 42}, {39}, {35}, {37, 79, 86}, {},
            {36, 42}, {41, 36, 32, 34, 47, 48, 43}, {42, 48}, {65, 45}, {44, 65, 89, 88, 46},
            {45, 88, 87}, {34, 42, 48, 50, 49}, {51, 50, 47, 42, 43}, {34, 47, 50, 54}, {49, 47, 48, 51, 55, 54},
            {55, 50, 48}, {}, {}, {49, 50, 55, 56}, {51, 50, 54, 56}, {54, 55, 62, 57}, {56, 61, 58}, {57, 61}, {63}, {},
            {58, 57, 62}, {61, 57, 56}, {59}, {}, {44, 45, 89, 69}, {}, {68}, {67}, {65, 70, 72, 90, 89}, {69, 72},
            {}, {70, 69, 90}, {}, {26, 27, 75, 76}, {27, 74, 76, 78, 77}, {74, 75, 78, 111, 110}, {75, 78, 112, 114, 115}, {75, 76, 111, 112, 77}, {39, 86, 81, 80}, {79, 81, 82},
            {86, 79, 80, 82, 93, 92}, {80, 81, 93, 84, 83}, {82, 84, 85}, {93, 82, 83, 85, 125, 124}, {83, 84, 124, 125}, {39, 79, 81, 92, 87}, {46, 88, 91, 92, 86}, {45, 46, 87, 91, 89}, {69, 65, 44, 45, 88, 91, 90}, {72, 69, 89, 91, 97},
            {90, 89, 88, 87, 92, 97}, {87, 86, 81, 93, 99, 98, 97, 91}, {92, 81, 82, 84, 124, 123, 99, 98}, {95}, {94}, {97, 100, 101}, {90, 91, 92, 98, 100, 96, 96}, {97, 92, 99, 121, 100}, {98, 93, 123, 122, 121}, {101, 96, 97, 98, 121},
            {96, 100, 121, 103, 102}, {101, 103, 104}, {105, 104, 102, 101, 121}, {102, 103, 105, 106}, {108, 106, 104, 103, 121}, {104, 105, 108, 113, 107}, {113, 109, 106}, {113, 106, 105, 121, 120, 119}, {110, 111, 112, 107}, {76, 111, 109},
            {78, 76, 110, 109, 112}, {114, 77, 78, 111, 109}, {107, 106, 108}, {117, 116, 115, 77, 112}, {116, 114, 77}, {115, 114, 117}, {116, 114}, {}, {108, 120}, {119, 108, 121, 130, 129},
            {120, 108, 105, 103, 101, 100, 98, 99, 122, 130}, {121, 99, 123, 130}, {122, 99, 93, 124, 126, 130}, {127, 126, 123, 93, 84, 85, 125}, {124, 85}, {128, 130, 122, 123, 124, 127}, {128, 126, 124}, {129, 130, 126, 127}, {120, 130, 128}, {120, 121, 122, 123, 126, 128, 129}
    };

    private final int[][] adjacencyByWater = {{} /*case0*/,
            {17, 3, 2, 5, 6, 53, 56, 57, 58, 73, 26, 25, 21, 19}, {4, 5, 1, 3}, {4, 2, 15, 17}, {3, 2, 15, 5}, {15, 7, 6, 4, 1, 2}, {8, 5, 16, 49, 53, 1}, {9, 5, 15}, {10, 16, 6}, {12, 11, 7, 15}, {},
            {9, 12}, {11, 9, 15, 13}, {14, 12, 15}, {13, 15}, {17, 18, 14, 13, 12, 9, 7, 5, 4, 3}, {6, 8, 10, 29, 33, 34, 49}, {18, 19, 1, 3, 15}, {17, 15}, {17, 20, 21, 1}, {19, 21},
            {19, 1, 25}, {}, {}, {}, {21, 26, 1}, {25, 1, 73, 74}, {}, {}, {10, 33, 16}, {},
            {}, {}, {29, 34, 16}, {33, 16, 49}, {37, 38, 36}, {41, 35, 38, 40}, {35, 38, 40, 39}, {35, 36, 40, 37}, {37, 86, 40}, {43, 42, 41, 36, 38, 37, 39, 86, 87, 46, 45, 44},
            {36, 42, 40}, {41, 43, 40}, {42, 40, 44, 52, 48}, {43, 52, 60, 64, 45, 40}, {44, 40, 46}, {45, 40, 87}, {}, {51, 52, 43}, {34, 16, 54, 53}, {},
            {55, 48, 52, 59, 60}, {51, 48, 43, 44, 60}, {1, 6, 49, 54, 56}, {49, 53, 56}, {56, 59, 51}, {54, 1, 57, 55, 59, 62}, {1, 56, 58}, {1, 57, 61, 67}, {55, 51, 60, 56}, {68, 66, 63, 59, 51, 52, 44, 64, 69, 70, 71},
            {58, 67, 66, 62}, {61, 66, 63, 59, 56}, {59, 62, 66, 60}, {60, 69, 65, 44}, {64, 44, 69}, {67, 61, 62, 63, 60, 68}, {73, 1, 58, 61, 66, 68}, {73, 67, 66, 60, 71}, {64, 60, 70, 65}, {60, 69, 71, 72},
            {73, 68, 60, 70, 72}, {73, 71, 70, 90, 94}, {74, 26, 1, 67, 68, 71, 72, 94, 95, 101, 102, 104, 106, 107, 109, 110, 76}, {26, 73, 76}, {}, {74, 110}, {}, {}, {}, {82, 83},
            {}, {80, 83}, {82, 80}, {}, {}, {39, 87, 40}, {46, 40, 86}, {}, {}, {72, 94, 95, 96, 97},
            {}, {}, {}, {73, 72, 90, 95}, {94, 90, 96, 101, 73}, {101, 95, 90, 97}, {90, 96}, {}, {}, {},
            {96, 95, 73, 102}, {73, 104, 101}, {}, {102, 106, 73}, {}, {104, 73, 107}, {109, 73, 106}, {113, 119}, {110, 73, 107}, {76, 73, 109},
            {}, {113, 114}, {112, 119, 114}, {112, 117, 118, 119, 113}, {}, {}, {114, 118}, {117, 114, 119}, {118, 114, 113, 108}, {},
            {}, {}, {}, {}, {124, 127, 129}, {}, {124, 125, 129, 128}, {129, 127}, {125, 128, 127}, {}
    };

    private ArrayList<Integer> volcanoes =  new ArrayList<>(Arrays.asList (2,4,5,7,66));

    private ArrayList<Integer> floodPlains = new ArrayList<>(Arrays.asList (10,11,36,41,42,77,78,109,110,111,123,124,125,126,127,128,130));

    public ArrayList<Integer> citySites = new ArrayList<>(Arrays.asList (2,3,6,7,9,13,17,19,21,26,41,42,43,44,49,51,53,57,58,59,61,62,63,65,66,67,68,70,71,78,87,88,89,91,93,95,96,99,100,102,103,104,105,106,108,109,110,111,115,116,122,125,126,127,128));

    public ArrayList<Integer> whiteCitySites = new ArrayList<>(Arrays.asList (41,78,109,110,111,126,127,128));

    private final int[] maxPawnsList = {0,
            0,1,2,2,2,1,1,2,2,3,
            2,1,1,1,0,0,3,2,3,2,
            1,2,1,1,1,3,1,1,4,5,
            1,3,2,2,3,5,1,2,2,0,
            2,4,3,2,2,1,1,2,1,1,
            1,1,1,1,2,2,2,1,2,0,
            1,2,1,1,2,2,2,3,3,1,
            2,1,0,2,1,2,4,5,1,1,
            3,1,2,3,2,1,2,2,2,2,
            2,3,3,2,1,2,2,4,3,2,
            3,1,2,3,1,2,1,1,3,4,
            3,1,1,2,3,3,1,0,1,1,
            3,2,3,4,3,2,3,3,2,3
    };

    public int posCityPawn[] = {
            0,0,
            0,0,
            242,665,// 2
            181,643,// 3
            0,0,
            0,0,
            294,497,// 6
            177,488,// 7
            0,0,
            88,440,// 9
            0,0,
            0,0,
            0,0,
            6,512,// 13
            0,0,
            0,0,
            0,0,
            57,706,// 17
            0,0,
            82,765,// 19
            0,0,
            254,895,// 21
            0,0,
            0,0,
            0,0,
            0,0,
            550,822,// 26
            0,0,
            0,0,
            0,0,
            0,0,
            0,0,
            0,0,
            0,0,
            0,0,
            0,0,
            0,0,
            0,0,
            0,0,
            0,0,
            0,0,
            622,212,// 41
            615,268,// 42
            687,357,// 43
            630,435,// 44
            0,0,
            0,0,
            0,0,
            0,0,
            373,465,// 49
            0,0,
            514,456,// 51
            0,0,
            386,514,// 53
            0,0,
            0,0,
            0,0,
            507,573,// 57
            511,613,// 58
            555,528,// 59
            0,0,
            519,588,// 61
            551,558,// 62
            568,536,// 63
            0,0,
            714,485,// 65
            641,626,// 66
            629,674,// 67
            651,665,// 68
            0,0,
            695,543,// 70
            737,583,// 71
            0,0,
            0,0,
            0,0,
            0,0,
            0,0,
            0,0,
            983,916,// 78
            0,0,
            0,0,
            0,0,
            0,0,
            0,0,
            0,0,
            0,0,
            0,0,
            1046,216,// 87
            892,323,// 88
            809,399,// 89
            0,0,
            971,360,// 91
            0,0,
            1274,334,// 93
            0,0,
            964,546,// 95
            1028,478,// 96
            0,0,
            0,0,
            1286,371,// 99
            1061,465,// 100
            0,0,
            1054,607,// 102
            1097,595,// 103
            1052,633,// 104
            1087,669,// 105
            1075,683,// 106
            0,0,
            1115,729,// 108
            974,758,// 109
            931,779,//110
            956,819,// 111
            0,0,
            0,0,
            0,0,
            1123,1040,// 115
            1109,999,// 116
            0,0,
            0,0,
            0,0,
            0,0,
            0,0,
            1234,460,// 122
            0,0,
            0,0,
            1554,401,// 125
            1396,455,// 126
            1466,475,// 127
            1500,499,// 128
            0,0,
            0,0
    };

    public int posPawn[] = {
            0,0,
            367,742,// 1
            250,697,
            195,658,
            261,618,
            279,567,
            320,501,
            182,490,
            222,444,
            94,439,
            112,349,// 10
            34,337,
            22,449,
            28,530,
            28,593,
            114,572,
            243,403,
            85,707,
            22,739,
            101,798,
            133,916,// 20
            266,912,
            27,881,
            91,1024,
            338,1044,
            449,959,
            550,852,
            624,1002,
            44,255,
            166,218,
            179,86,// 30
            417,39,
            413,172,
            234,339,
            339,340,
            661,49,
            562,189,
            916,55,
            797,114,
            1026,69,
            831,196,// 40
            649,206,
            576,292,
            652,365,
            697,395,
            798,336,
            862,262,
            436,346,
            558,376,
            381,441,
            460,414,// 50
            529,453,
            585,445,
            392,530,
            420,496,
            484,494,
            480,548,
            480,590,
            517,644,
            550,517,
            635,543,// 60
            555,616,
            572,573,
            595,550,
            632,499,
            707,462,
            628,612,
            613,677,
            688,659,
            755,491,
            740,543,// 70
            739,599,
            826,552,
            830,690,
            712,840,
            821,1008,
            859,866,
            1005,1016,
            971,928,
            1147,47,
            1344,40,// 80
            1235,155,
            1372,152,
            1518,117,
            1409,275,
            1552,282,
            1107,162,
            1011,229,
            927,325,
            836,407,
            915,485,// 90
            980,375,
            1125,279,
            1281,299,
            933,580,
            987,543,
            1043,498,
            1041,405,
            1144,382,
            1251,395,
            1106,465,// 100
            1097,540,
            1054,599,
            1141,592,
            1082,645,
            1157,684,
            1069,709,
            1027,742,
            1170,773,
            976,771,
            909,785,// 110
            958,846,
            1044,870,
            1083,812,
            1112,932,
            1093,1049,
            1156,1030,
            1246,1030,
            1305,991,
            1375,943,
            1452,778,// 120
            1241,589,
            1286,499,
            1342,431,
            1434,386,
            1552,407,
            1426,467,
            1490,468,
            1483,534,
            1545,571,
            1383,535// 130
    };

    public Map(Context c) {
        //mapImg = BitmapFactory.decodeResource (c.getResources ( ), R.drawable.carte);
        try {
            assetInStream = c.getAssets().open("color.png");
        } catch (IOException e) {
            e.printStackTrace ( );
        }
        colorImg = BitmapFactory.decodeStream(assetInStream);


        //Map areas generator
        areas[0] = new Area(0);
        for (int i = 1; i <= 130; i++) {
            Area tempArea = new Area(i, listWater[i], listLand[i], volcanoes.contains (i), floodPlains.contains (i), citySites.contains (i), whiteCitySites.contains (i), maxPawnsList[i], adjacencyByLand[i], adjacencyByWater[i],posPawn[2*i],posPawn[(2*i)+1],posCityPawn[2*i],posCityPawn[(2*i)+1]);
            areas[i] = tempArea;
        }

    }

    public Area whichArea(Context c, float ax, float ay) {

        float density = c.getResources ( ).getDisplayMetrics ( ).density;

        int h = colorImg.getHeight ( );
        int d = (int) (h / 1089);
        int px = (int) (ax * d / density);
        int py = (int) (ay * d / density);
        int col = colorImg.getPixel (px, py);
        String r = String.valueOf (Color.red (col));
        String g = String.valueOf (Color.green (col));
        String b = String.valueOf (Color.blue (col));

        return areas[Color.blue (col)];
    }


}
