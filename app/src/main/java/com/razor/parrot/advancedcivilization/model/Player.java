package com.razor.parrot.advancedcivilization.model;

import android.os.Parcelable;
import android.os.Parcel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

public class Player implements Parcelable {

    final public int num;
    public Civilization civilization;
    public ArrayList<Area> cities = new ArrayList<>(9);
    public ArrayList<Area> ships = new ArrayList<> (4);
    public ArrayList<cartes> cartesdujoueur=new ArrayList<>();
    public String motdepasse;
    public int treasury = 0;
    public int stock = 55;
    public int census = 0;
    public int cityPawns = 9;
    public int shipPawns = 4;
    public int astPlace = 1;

    public Player(int num) {
        this.num = num;
    }

    protected Player(Parcel in) {
        num = in.readInt();
        civilization = (Civilization) in.readValue(Civilization.class.getClassLoader());
        if (in.readByte() == 0x01) {
            cities = new ArrayList<Area>();
            in.readList(cities, Area.class.getClassLoader());
        } else {
            cities = null;
        }
        if (in.readByte() == 0x01) {
            ships = new ArrayList<Area>();
            in.readList(ships, Area.class.getClassLoader());
        } else {
            ships = null;
        }
        if (in.readByte() == 0x01) {
            cartesdujoueur = new ArrayList<cartes>();
            in.readList(cartesdujoueur, cartes.class.getClassLoader());
        } else {
            cartesdujoueur = null;
        }
        motdepasse = in.readString();
        treasury = in.readInt();
        stock = in.readInt();
        census = in.readInt();
        cityPawns = in.readInt();
        shipPawns = in.readInt();
        astPlace = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(num);
        dest.writeValue(civilization);
        if (cities == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(cities);
        }
        if (ships == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(ships);
        }
        if (cartesdujoueur == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(cartesdujoueur);
        }
        dest.writeString(motdepasse);
        dest.writeInt(treasury);
        dest.writeInt(stock);
        dest.writeInt(census);
        dest.writeInt(cityPawns);
        dest.writeInt(shipPawns);
        dest.writeInt(astPlace);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Player> CREATOR = new Parcelable.Creator<Player>() {
        @Override
        public Player createFromParcel(Parcel in) {
            return new Player(in);
        }

        @Override
        public Player[] newArray(int size) {
            return new Player[size];
        }
    };
}
