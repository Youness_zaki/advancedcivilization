package com.razor.parrot.advancedcivilization.view_and_controller;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.razor.parrot.advancedcivilization.R;
import com.razor.parrot.advancedcivilization.model.Map;

public class MainActivity extends AppCompatActivity {

    private Button start;
    private SeekBar sbNbPlayers;
    private TextView nb;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        start = (Button) findViewById (R.id.startButton);
        sbNbPlayers = (SeekBar) findViewById (R.id.seekBarPlayers);
        nb = (TextView) findViewById (R.id.textViewNb);

        nb.setText ("1");

        //final Controller controller = new Controller (this);
        //controller.initialise ();

        SeekBar.OnSeekBarChangeListener lNbPlayers = new SeekBar.OnSeekBarChangeListener ( ) {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                nb.setText (String.valueOf(seekBar.getProgress ()));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                nb.setText (String.valueOf(seekBar.getProgress ()));
            }
        };

        sbNbPlayers.setOnSeekBarChangeListener (lNbPlayers);


        start.setOnClickListener (new View.OnClickListener ( ) {
            @Override
            public void onClick(View v) {
                int nbPlayers = sbNbPlayers.getProgress();
                if (nbPlayers <= 1){
                    nb.setText ("You have to be more than 1 to play !");
                }
                else {
                    Intent intent = new Intent(MainActivity.this, MapViewActivity.class);
                    intent.putExtra ("NbPlayers",nbPlayers);
                    startActivity (intent);
                }
            }
        });
    }

}
